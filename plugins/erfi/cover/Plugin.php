<?php namespace Erfi\Cover;

/**
 * The plugin.php file (called the plugin initialization script) defines the plugin information class.
 */

use System\Classes\PluginBase;

class Plugin extends PluginBase
{

    public function pluginDetails()
    {
        return [
            'name'        => 'Cover Image Plugin',
            'description' => 'Add Cover Image to your posts',
            'author'      => 'Alexey Bobkov, Samuel Georges',
            'icon'        => 'icon-picture-o',
            'homepage'    =>  'erfanghoreishi.me'
        ];
    }

  
    public function registerComponents()
    {
        return [
            'Erfi\Cover\Components\Cover' => 'coverImage'
        ];
    }
}
