<?php namespace Erfi\Cover\Components;

use Cms\Classes\ComponentBase;
use ApplicationException;

class Cover extends ComponentBase
{

    public function componentDetails()
    {
        return [
            'name'        => 'Cover Image',
            'description' => 'Add cover image to post .'
        ];
    }

    public function defineProperties()
    {
        return [
            'maxItems' => [
                 'title'             => 'Max items',
                 'description'       => 'The most amount of todo items allowed',
                 'default'              => 10,
                 'type'              => 'file',
                 'validationPattern' => '^[0-9]+$',
                 'validationMessage' => 'The Max Items property can contain only numeric symbols'
            ]
        ];
    }


    public function onRun(){
        //$link=
        
    }

}
